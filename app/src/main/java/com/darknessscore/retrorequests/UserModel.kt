package com.darknessscore.retrorequests

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserModel : Model{

    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("age")
    @Expose
    var age: String? = null

    @SerializedName("address")
    @Expose
    var address: String? = null
}
