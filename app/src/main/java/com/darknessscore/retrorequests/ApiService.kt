package com.darknessscore.retrorequests

import retrofit2.Call
import retrofit2.http.*

/**
 * Created by Евгений on 23.02.2018.
 */
interface ApiService<T : Model> {
    @GET("/api/values/{id}")
    fun getEntry(@Path("id") id: Int): Call<T>

    @GET("/api/values")
    fun getEntries(): Call<List<T>>

    @DELETE("/api/values/{id}")
    fun removeEntry(@Path("id") id: Int): Call<T>

    @Headers(
            "Accept: application/json",
            "Content-Type: application/json")
    @POST("/api/values")
    fun postEntry(@Body model: T): Call<T>

    @Headers(
            "Accept: application/json",
            "Content-Type: application/json")
    @PUT("/api/values/{id}")
    fun replaceEntry(@Path("id") id: Int, @Body mode: T): Call<T>
}