package com.darknessscore.retrorequests

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit
import android.widget.Toast
import com.darknessscore.retrorequests.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

class MainActivity : AppCompatActivity() {
    val BASE_URL = "http://projectkerosweb.azurewebsites.net/"
    lateinit var testTextView : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        testTextView = findViewById(R.id.textView);


        val o = RequestManager<UserModel>("http://projectkerosweb.azurewebsites.net/")
        Log.i("QWERTY",o.apiListener.toString())

        o.apiListener.postEntry(UserModel().apply { this.name = "try to do awesome" }).enqueue(object : Callback<UserModel> {

            override fun onResponse(call: Call<UserModel>, response: Response<UserModel>) {
                // Log.i("reque", call.)
                Log.i("respo", response.toString())
                Log.i("respo", response.body().toString())
                Log.i("respo", response.headers().get("Location"))
                testTextView.text = (response.body() as UserModel).name
            }

            override fun onFailure(call: Call<UserModel>, t: Throwable) {

                t.printStackTrace()
                Toast.makeText(this@MainActivity, "An error occurred during networking", Toast.LENGTH_SHORT).show()
            }
        })
        /*kerosApiListener.getData(19).enqueue(object : Callback<UserModel> {
            override fun onResponse(call: Call<UserModel>, response: Response<UserModel>) {
                testTextView.text = (response.body() as UserModel).name
            }

            override fun onFailure(call: Call<UserModel>, t: Throwable) {
                Toast.makeText(this@MainActivity, "An error occurred during networking", Toast.LENGTH_SHORT).show()
            }
        });*/
    }

    fun postCall(v : View){
        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                //     .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        val kerosApiListener = retrofit.create(KerosApiService::class.java)


       kerosApiListener.postData(UserModel().apply { this.name = "Eugene"; this.age = "3"; }).enqueue(
                object : Callback<UserModel> {

                    override fun onResponse(call: Call<UserModel>, response: Response<UserModel>) {
                       // Log.i("reque", call.)
                        Log.i("respo", response.toString())
                        Log.i("respo", response.body().toString())
                        Log.i("respo", response.headers().get("Location"))
                          testTextView.text = (response.body() as UserModel).name
                    }

                    override fun onFailure(call: Call<UserModel>, t: Throwable) {

                        t.printStackTrace()
                        Toast.makeText(this@MainActivity, "An error occurred during networking", Toast.LENGTH_SHORT).show()
                    }
                }
        )
    }

    fun putCall(v : View){
        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                //     .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        val kerosApiListener = retrofit.create(KerosApiService::class.java)


        kerosApiListener.putData( 19 ,UserModel().apply {this.name = "Eugene!"; this.age = "3"; }).enqueue(
                object : Callback<UserModel> {

                    override fun onResponse(call: Call<UserModel>, response: Response<UserModel>) {
                        // Log.i("reque", call.)
                        Log.i("respo", response.toString())
                        Log.i("respo", response.body().toString())
                      //  Log.i("respo", response.headers().get("Location"))
                        testTextView.text = "puted"
                    }

                    override fun onFailure(call: Call<UserModel>, t: Throwable) {

                        t.printStackTrace()
                        Toast.makeText(this@MainActivity, "An error occurred during networking", Toast.LENGTH_SHORT).show()
                    }
                }
        )
    }

    fun delCall(v : View){
        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                //     .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        val kerosApiListener = retrofit.create(KerosApiService::class.java)

        kerosApiListener.delData(19).enqueue(object : Callback<UserModel>{
            override fun onResponse(call: Call<UserModel>, response: Response<UserModel>) {
                testTextView.text = "deleted"
            }

            override fun onFailure(call: Call<UserModel>, t: Throwable) {

                t.printStackTrace()
                Toast.makeText(this@MainActivity, "An error occurred during networking", Toast.LENGTH_SHORT).show()
            }
        })
    }

    fun getCall(v : View){
        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
           //     .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        var kerosApiListener = retrofit.create(KerosApiService::class.java)

        kerosApiListener.getData(19).enqueue(object : Callback<UserModel>{
            override fun onResponse(call: Call<UserModel>, response: Response<UserModel>) {
                testTextView.text = (response.body() as UserModel).name
            }

            override fun onFailure(call: Call<UserModel>, t: Throwable) {

                t.printStackTrace()
                Toast.makeText(this@MainActivity, "An error occurred during networking", Toast.LENGTH_SHORT).show()
            }
        })
               /* object : Callback<List<UserModel>> {
            override fun onResponse(call: Call<List<UserModel>>, response: Response<List<UserModel>>) {
                testTextView.text = (response.body() as ArrayList<UserModel>)[0].name
            }

            override fun onFailure(call: Call<List<UserModel>>, t: Throwable) {

                t.printStackTrace()
                Toast.makeText(this@MainActivity, "An error occurred during networking", Toast.LENGTH_SHORT).show()
            }
        })*/
    }
}
