package com.darknessscore.retrorequests

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type

/**
 * Created by Евгений on 23.02.2018.
 */
class RequestManager<T : Model>(url : String) {

    var retrofit : Retrofit
    var apiListener : ApiService<T>
    val cl : T? = null

    init {
        retrofit = Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                //     .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

                 apiListener = retrofit.create(ApiService::class.java) as ApiService<T>

    }




}