package com.darknessscore.retrorequests

import retrofit2.Call
import retrofit2.http.*


interface KerosApiService {
    @GET("/api/values/{id}")
    fun getData(@Path("id") id: Int): Call<UserModel>

    @DELETE("/api/values/{id}")
    fun delData(@Path("id") id: Int): Call<UserModel>


    @GET("/api/values/")
    fun getDataAr(): Call<List<UserModel>>

    @Headers(
            "Accept: application/json",
            "Content-Type: application/json")
    @POST("/api/values")
    fun postData(@Body user: UserModel): Call<UserModel>

    @Headers(
            "Accept: application/json",
            "Content-Type: application/json")
    @PUT("/api/values/{id}")
    fun putData(@Path("id") id: Int, @Body user: UserModel): Call<UserModel>

}